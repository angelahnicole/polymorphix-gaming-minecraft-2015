Want more information about the BukkitGames mod and its commands? Click <a href="https://www.spigotmc.org/resources/bukkitgames-hungergames.279/">here</a> to access its official Spigot page! <br><br>

<!-- HUNGER GAMES COMMANDS TABLE -->
<table class="table table-striped table-condensed commands" cellspacing="0" width="100%">

    <colgroup>
        <col class="width4" />
        <col class="width4" />
        <col class="width4" />
        <col class="width4" />
    </colgroup>

    <thead class="bigger-font">
        <tr>
            <th>Command Description</th>
            <th>Command Text</th> 
            <th>Command Parameters</th>                         
            <th>Command Example</th>

        </tr>
    </thead>

    <tbody>
        
        <tr>
            <td>Teleport to the given server</td>
            <td><code>/server {server name}</code></td>
            <td>
                <ul>
                    <li><b>server name</b>: plot, pvp, mcmmo</li>
                </ul>
            </td>
            <td><code>/server pvp</code></td>
        </tr>
        <tr>
            <td>Vote for the next map to used in the next round</td>
            <td><code>/bg vote {map id} </code></td>
            <td>
                <ul>
                    <li><b>map id</b>: number that represents the map</li>
                </ul>
            </td>
            <td><code>/bg vote 1</code></td>  
        </tr>  

    </tbody>

</table>
<!-- /HUNGER GAMES COMMANDS TABLE -->