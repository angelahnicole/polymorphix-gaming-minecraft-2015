Want more information about the PlotSquared mod and its commands? Click <a href="https://github.com/IntellectualSites/PlotSquared/wiki">here</a> to access its official wiki!  <br><br>

<!-- PLOTWORLD COMMANDS TABLE -->
<table class="table table-striped table-condensed commands" cellspacing="0" width="100%">

    <colgroup>
        <col class="width4" />
        <col class="width4" />
        <col class="width4" />
        <col class="width4" />
    </colgroup>

    <thead class="bigger-font">
        <tr>
            <th>Command Description</th>
            <th>Command Text</th> 
            <th>Command Parameters</th>                         
            <th>Command Example</th>

        </tr>
    </thead>

    <tbody>

        <tr>
            <td>Teleport to the spawn point</td>
            <td><code>/spawn</code></td>
            <td>N/A</td>
            <td><code>/spawn</code></td>
        </tr>
        <tr>
            <td>Teleport to the given server</td>
            <td><code>/server {server name}</code></td>
            <td>
                <ul>
                    <li><b>server name</b>: plot, pvp, mcmmo</li>
                </ul>
            </td>
            <td><code>/server pvp</code></td>
        </tr>
        <tr>
            <td>Claim the nearest plot</td>
            <td><code>/plot auto</code></td>
            <td>N/A</td>
            <td><code>/plot auto</code></td>  
        </tr> 
        <tr>
            <td>Claim a plot</td>
            <td><code>/plot claim</code></td>
            <td>N/A</td>
            <td><code>/plot claim</code></td>  
        </tr> 
        <tr>
            <td>List all plots for a category</td>
            <td><code>/plot list {category} </code></td>
            <td>
                <ul>
                    <li><b>category</b>: mine, shared, world, all</li>
                </ul>
            </td>
            <td><code>/plot list all </code></td>  
        </tr>  
        <tr>
            <td>Rate a plot</td>
            <td><code>/plot rate {rating} </code></td>
            <td>
                <ul>
                    <li><b>rating</b>: 1-10</li>
                </ul>
            </td>
            <td><code>/plot rate 9</code></td>  
        </tr>  
        <tr>
            <td>Display plot information</td>
            <td><code>/plot info {category} </code></td>
            <td>
                <ul>
                    <li><b>category</b> <i>(optional)</i>: helpers, trusted, denied, flags, biome, rating, owner, id, alias, size</li>
                </ul>
            </td>
            <td><code>/plot info </code></td>  
        </tr>  
        <tr>
            <td>Trust a player to work on your plot</td>
            <td><code>/plot trust {player}</code></td>
            <td>
                <ul>
                    <li><b>player</b>: username of player</li>
                </ul>
            </td>
            <td><code>/plot trust evalka</code></td>  
        </tr>  
        <tr>
            <td>Add a player to build while the owner of the plot is still online</td>
            <td><code>/plot add {player} </code></td>
            <td>
                <ul>
                    <li><b>player</b>: username of player</li>
                </ul>
            </td>
            <td><code>/plot add evalka </code></td>  
        </tr>  
        <tr>
            <td>Set the plot alias/name</td>
            <td><code>/plot set alias {name} </code></td>
            <td>
                <ul>
                    <li><b>name</b>: one word string</li>
                </ul>
            </td>
            <td><code>/plot set alias narnia</code></td>  
        </tr>  
        <tr>
            <td>Set the plot home to your current location</td>
            <td><code>/plot set home</code></td>
            <td>N/A</td>
            <td><code>/plot set home</code></td>  
        </tr>
        <tr>
            <td>Teleport to a plot</td>
            <td><code>/plot tp {location} </code></td>
            <td>
                <ul>
                    <li><b>location</b>: alias, id, player</li>
                </ul>
            </td>
            <td><code>/plot tp narnia </code></td>  
        </tr>
        <tr>
            <td>Visit someone's plot</td>
            <td><code>/plot visit {player} </code></td>
            <td>
                <ul>
                    <li><b>player</b>: username of player</li>
                </ul>
            </td>
            <td><code>/plot visit evalka </code></td>  
        </tr>
        <tr>
            <td>Go to your plot</td>
            <td><code>/plot home {location} </code></td>
            <td>
                <ul>
                    <li><b>location</b> <i>(optional)</i>: alias, id</li>
                </ul>
            </td>
            <td><code>/plot home </code></td>  
        </tr>
        <tr>
            <td>Set the biome for the plot</td>
            <td><code>/plot set biome {biome} </code></td>
            <td>
                <ul>
                    <li><b>biome</b>: biome to set your plot to</li>
                </ul>
            </td>
            <td><code>/plot set biome forest</code></td>  
        </tr>  
        <tr>
            <td>Set the plot floor</td>
            <td><code>/plot set floor {block id} </code></td>
            <td>
                <ul>
                    <li><b>block id</b>: id of desired block <i>(click <a href="http://www.minecraftinfo.com/idnamelist.htm">here</a> for a list of block IDs)</i></li>
                </ul>
            </td>
            <td><code>/plot set floor 57</code></td>  
        </tr>  
        <tr>
            <td>Clear your current plot</td>
            <td><code>/plot clear </code></td>
            <td>N/A</td>
            <td><code>/plot clear </code></td>  
        </tr>
        <tr>
            <td>Delete your current plot</td>
            <td><code>/plot delete </code></td>
            <td>N/A</td>
            <td><code>/plot delete </code></td>  
        </tr>
        <tr>
            <td>Set a flag in the current plot</td>
            <td><code>/plot set flag {flag} {key-value} </code></td>
            <td>
                <ul>
                    <li><b>flag</b> <i>notifications</i>: titles, greeting, farewell, notify-enter, notify-leave</li>
                    <li><b>flag</b> <i>attributes</i>: feed, heal, invincible, instabreak, fly, gamemode, time, weather, music, disable-physics</li>
                    <li><b>key-value</b>: key-value parameter for different types of flags</li>
                </ul>
            </td>
            <td><code>/plot set flag greeting welcome to narnia!</code></td>  
        </tr>

    </tbody>

</table>
<!-- /PLOTWORLD COMMANDS TABLE -->