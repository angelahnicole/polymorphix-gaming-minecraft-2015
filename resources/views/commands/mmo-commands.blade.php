Want more information about the McMMO mod and its commands? Click <a href="https://github.com/mcMMO-Dev/mcMMO/wiki/Play">here</a> to access its official wiki! <br><br>

<!-- MMO COMMANDS TABLE -->
<table class="table table-striped table-condensed commands" cellspacing="0" width="100%">

    <colgroup>
        <col class="width4" />
        <col class="width4" />
        <col class="width4" />
        <col class="width4" />
    </colgroup>

    <thead class="bigger-font">
        <tr>
            <th>Command Description</th>
            <th>Command Text</th> 
            <th>Command Parameters</th>                         
            <th>Command Example</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>List all of the commands available to the player</td>
            <td><code>/rules</code></td>
            <td>N/A</td>
            <td><code>/rules</code></td>
        </tr>
        <tr>
            <td>Set a waypoint to teleport to</td>
            <td><code>/sethome</code></td>
            <td>N/A</td>
            <td><code>/sethome</code></td>
        </tr>
        <tr>
            <td>Teleport to the waypoint you set</td>
            <td><code>/home</code></td>
            <td>N/A</td>
            <td><code>/home</code></td>
        </tr>
        <tr>
            <td>Teleport to the spawn point</td>
            <td><code>/spawn</code></td>
            <td>N/A</td>
            <td><code>/spawn</code></td>
        </tr>
        <tr>
            <td>Teleport to your last point of death</td>
            <td><code>/back</code></td>
            <td>N/A</td>
            <td><code>/back</code></td>
        </tr>
        <tr>
            <td>Send request to a player asking for teleportation</td>
            <td><code>/tpa {player}</code></td>
            <td>
                <ul>
                    <li><b>player</b>: username of player</li>
                </ul>
            </td>
            <td><code>/tpa evalka</code></td>
        </tr>
        <tr>
            <td>Send a request to the player asking if they can teleport to your location</td>
            <td><code>/tpahere {player}</code></td>
            <td>
                <ul>
                    <li><b>player</b>: username of player</li>
                </ul>
            </td>
            <td><code>/tpahere evalka</code></td>
        </tr>
        <tr>
            <td>Accept or deny a teleportation request</td>
            <td><code>/tpaccept</code> or <code>/tpdeny</code></td>
            <td>N/A</td>
            <td><code>/tpaccept</code></td>
        </tr>
        <tr>
            <td>Teleport to the given server</td>
            <td><code>/server {server name}</code></td>
            <td>
                <ul>
                    <li><b>server name</b>: plot, pvp, mcmmo</li>
                </ul>
            </td>
            <td><code>/server pvp</code></td>
        </tr>
        <tr>
            <td>Put a lock on a chest</td>
            <td><code>/cprivate {player names}</code></td>
            <td>
                <ul>
                    <li><b>player names</b>: username of players to grant access</li>
                </ul>
            </td>
            <td><code>/cprivate evalka rudeoddity</code></td>
        </tr>
        <tr>
            <td>Grant players access to a chest</td>
            <td><code>/cmodify {player names}</code></td>
            <td>
                <ul>
                    <li><b>player names</b>: username of players to grant access</li>
                </ul>
            </td>
            <td><code>/cmodify evalka rudeoddity</code></td>
        </tr>
        <tr>
            <td>Remove a lock on a chest</td>
            <td><code>/cremove</code></td>
            <td>N/A </td>
            <td><code>/cremove</code></td>
        </tr>
        <tr>
            <td>List top 10 players that has the highest powerlevel or skill</td>
            <td><code>/mctop {skill name} {page number}</code></td>
            <td>
                <ul>
                    <li><b>skill name</b> <i>(optional)</i>: acrobatics, alchemy, archery, axes, excavation, fishing, herbalism, mining, taming, repair, swords, unarmed, wood cutting</li>
                    <li><b>page number</b> <i>(optional)</i>: specific page of the leader-boards</li>
                </ul>
            </td>
            <td><code>/mctop archery</code></td>  
        </tr>  
        <tr>
            <td>Check player's position on the server's leaderboard. If {player} is left blank, then this will check your own rankings</td>
            <td><code>/mcrank {player}</code></td>
            <td>
                <ul>
                    <li><b>player</b> <i>(optional)</i>: username of player</li>
                </ul>
            </td>
            <td><code>/mcrank evalka</code></td>  
        </tr>  
        <tr>
            <td>Check your own stats for every skill. This includes the skill-level and experience for each skill.</td>
            <td><code>/mcstats</code></td>
            <td>N/A</td>
            <td><code>/mcstats</code></td>  
        </tr>
        <tr>
            <td>Check player's position on the server's leaderboard. If {player} is left blank, then this will check your own rankings</td>
            <td><code>/mcrank {player}</code></td>
            <td>
                <ul>
                    <li><b>player</b> <i>(optional)</i>: username of player</li>
                </ul>
            </td>
            <td><code>/mcrank evalka</code></td>  
        </tr>
        <tr>
            <td>Create a party</td>
            <td><code>/party create {party name} {password}</code></td>
            <td>
                <ul>
                    <li><b>party name</b>: name of the party</li>
                    <li><b>password</b> <i>(optional)</i>: password to join the party</li>
                </ul>
            </td>
            <td><code>/party create narnia-party</code></td>  
        </tr>
        <tr>
            <td>Join a party</td>
            <td><code>/party join {party name} {password}</code></td>
            <td>
                <ul>
                    <li><b>party name</b>: name of the party</li>
                    <li><b>password</b> <i>(optional)</i>: password to join the party (if there is one set)</li>
                </ul>
            </td>
            <td><code>/party join narnia-party</code></td>  
        </tr>
        <tr>
            <td>Leave a party</td>
            <td><code>/party {quit aliases}</code></td>
            <td>
                <ul>
                    <li><b>quit aliases</b>: leave, quit, q</li>
                </ul>
            </td>
            <td><code>/party leave</code></td>  
        </tr>
        <tr>
            <td>Toggle party chat</td>
            <td><code>/party chat {message}</code> or <code>/pc {message}</code></td>
            <td>
                <ul>
                    <li><b>message</b> <i>(optional)</i>: message to send to party without toggling party chat</li>
                </ul>
            </td>
            <td><code>/party chat</code></td>  
        </tr>
        <tr>
            <td>Request a teleport to the target party-member player</td>
            <td><code>/ptp {player}</code> or <code>/party teleport {player}</code></td>
            <td>
                <ul>
                    <li><b>player</b>: username of player</li>
                </ul>
            </td>
            <td><code>/ptp evalka</code></td>  
        </tr>
        <tr>
            <td>Accept a teleport from a player</td>
            <td><code>/ptp accept</code> or <code>/party teleport accept</code></td>
            <td>N/A</td>
            <td><code>/ptp accept</code></td>  
        </tr>
        <tr>
            <td>Disable player teleport requests</td>
            <td><code>/ptp toggle</code> or <code>/party teleport toggle</code></td>
            <td>N/A</td>
            <td><code>/ptp toggle</code></td>  
        </tr>
        <tr>
            <td>Request to form an alliance with another party</td>
            <td><code>/party alliance invite {leader player}</code></td>
            <td>
                <ul>
                    <li><b>leader player</b>: username of player that is the party leader</li>
                </ul>
            </td>
            <td><code>/party alliance invite evalka</code></td> 
        </tr>
        <tr>
            <td>Accept request to form an alliance with another party</td>
            <td><code>/party alliance accept</code></td>
            <td>N/A</td>
            <td><code>/party alliance accept</code></td>
        </tr>
        <tr>
            <td>View alliance status</td>
            <td><code>/party alliance</code></td>
            <td>N/A</td>
            <td><code>/party alliance</code></td>
        </tr>
        <tr>
            <td>Disband party alliance</td>
            <td><code>/party alliance disband</code></td>
            <td>N/A</td>
            <td><code>/party alliance disband</code></td>
        </tr>
        <tr>
            <td>Activate a party item share mode</td>
            <td><code>/party itemshare {mode}</code></td>
            <td>
                <ul>
                    <li><b>mode</b>: random, equal, none</li>
                </ul>
            </td>
            <td><code>/party itemshare random</code></td> 
        </tr>
        <tr>
            <td>Activate an experience share mode</td>
            <td><code>/party xpshare {mode}</code></td>
            <td>
                <ul>
                    <li><b>mode</b>: equal, none</li>
                </ul>
            </td>
            <td><code>/party xpshare equal</code></td> 
        </tr>
        <tr>
            <td>Lock or unlock party</td>
            <td><code>/party {mode}</code></td>
            <td>
                <ul>
                    <li><b>mode</b>: lock, unlock</li>
                </ul>
            </td>
            <td><code>/party unlock</code></td> 
        </tr>
        <tr>
            <td>Change (or set) the party's password</td>
            <td><code>/party password {password}</code></td>
            <td>
                <ul>
                    <li><b>password</b>: password to join the party</li>
                </ul>
            </td>
            <td><code>/party password 1234</code></td> 
        </tr>
        <tr>
            <td>Change the party's name</td>
            <td><code>/party rename {party name}</code></td>
            <td>
                <ul>
                    <li><b>party name</b>: name of the party</li>
                </ul>
            </td>
            <td><code>/party rename dream-team</code></td> 
        </tr>
        <tr>
            <td>Change the party's leader</td>
            <td><code>/party owner {player}</code></td>
            <td>
                <ul>
                    <li><b>player</b>: username of player</li>
                </ul>
            </td>
            <td><code>/party owner evalka</code></td> 
        </tr>
        <tr>
            <td>Disband the party</td>
            <td><code>/party disband</code></td>
            <td>N/A</td>
            <td><code>/party disband</code></td> 
        </tr>


    </tbody>

</table>
<!-- /MMO COMMANDS TABLE -->

