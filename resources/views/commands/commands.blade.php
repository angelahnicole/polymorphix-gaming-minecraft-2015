
<div class="container-fluid">

    <div class="row">
        <div class="col-md-1"></div>

        <div class="thumbnail with-caption col-md-10">
            <img class="img-responsive img-thumbnail" src="{{ URL::asset('assets/imx/frontpage/server-commands_photo.png') }}">
            <p><small>Image by <a href="http://www.deviantart.com/art/Redstone-357482917">Juandi-Hitokiri</a></small></p>
        </div>

        <div class="col-md-1"></div>
    </div>
    
    <div class="row">
        <div class="col-md-1"></div>

        <!-- SERVER COMMANDS -->
        <div class="col-md-10">
            
            <!-- SERVER COMMAND NAVIGATION TABS -->
            <ul class="nav nav-tabs front-page-nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#mmo-commands" aria-controls="mmo-commands" role="tab" data-toggle="tab">MMO Commands</a></li>
                <li role="presentation"><a href="#hunger-games-commands" aria-controls="hunger-games-commands" role="tab" data-toggle="tab">Hunger Games Commands</a></li>
                <li role="presentation"><a href="#plot-world-commands" aria-controls="plot-world-commands" role="tab" data-toggle="tab">PlotWorld Commands</a></li>
            </ul>
            <!-- /SERVER COMMAND NAVIGATION TABS -->

            <!-- SERVER COMMAND NAVIGATION TAB PANES-->
            <div class="tab-content front-page-nav-command-tabs">

                <div role="tabpanel" class="tab-pane active" id="mmo-commands">

                    @include('commands.mmo-commands')

                </div>

                <div role="tabpanel" class="tab-pane" id="hunger-games-commands">

                    @include('commands.hunger-games-commands')

                </div>

                <div role="tabpanel" class="tab-pane" id="plot-world-commands">

                    @include('commands.plot-commands')

                </div>

            </div>
            <!-- /SERVER COMMAND NAVIGATION TAB PANES-->
            
        </div>
         <!-- /SERVER COMMANDS -->

        <div class="col-md-1"></div>
    </div>
    
</div>

