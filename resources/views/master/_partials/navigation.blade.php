<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ url('/') }}"><span class="glyphicon icon-polymorphix"></span>Polymorphix Gaming </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    
    @if( isset($isMain) && $isMain )
      <ul class="nav navbar-nav" id="#main-nav">
        <li><a href="#" onClick="$('#minecraft').animatescroll();" class="front-nav active" id="minecraft-nav"><span class="glyphicon icon-minecraft"></span> Minecraft 2015</a></li>
        <li><a href="#" onClick="$('#mmo').animatescroll();" class="front-nav" id="mmo-nav"><span class="glyphicon icon-mmo"></span>MMO</a></li>
        <li><a href="#" onClick="$('#hunger-games').animatescroll();" class="front-nav" id="hunger-games-nav"><span class="glyphicon icon-hungergames"></span>Hunger Games</a></li>
        <li><a href="#" onClick="$('#plot-world').animatescroll();" class="front-nav" id="plot-world-nav"><span class="glyphicon icon-plotworld"></span>Plot World</a></li>
        <li><a href="#" onClick="$('#server-commands').animatescroll();" class="front-nav" id="server-commands-nav"><span class="glyphicon icon-terminal"></span>Server Commands</a></li>
      </ul> 
    @endif
        
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>