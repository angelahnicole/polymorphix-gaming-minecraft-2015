
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-1"></div>
        
        <div class="thumbnail with-caption col-md-10">
            <img class="img-responsive img-thumbnail" src="{{ URL::asset('assets/imx/frontpage/minecraft_photo.jpg') }}">
            <p><small>Image by <a href="http://johntuley.deviantart.com/art/Minecraft-The-Other-Side-324734597">JohnTuley</a></small></p>
        </div>
        
        <div class="col-md-1"></div>
    </div>
    
    <div class="row">       
        <div class="col-md-1"></div>
        
        <!-- IFRAME DYNMAPS -->
        <div class="col-md-10">
            
            <!-- IFRAME DYNMAP TAB NAVIGATION -->
            <ul class="nav nav-tabs front-page-nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#mmo-map" aria-controls="mmo-map" role="tab" data-toggle="tab">MMO Map</a></li>
                <li role="presentation"><a href="#plot-world-map" aria-controls="plot-world-map" role="tab" data-toggle="tab">PlotWorld Map</a></li>
            </ul>
            <!-- /IFRAME DYNMAP TAB NAVIGATION -->

            <!-- IFRAME DYNAMP TAB PANES -->
            <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="mmo-map">

                    <div class="embed-responsive embed-responsive-4by3">   
                      <object data="http://polymorphixgaming.com:8123/"> 
                          Object could not be rendered. You can go to the dynmap manually <a href="http://polymorphixgaming.com:8123/">here</a>, but the server may be down.
                      </object>
                    </div>

                </div>

                <div role="tabpanel" class="tab-pane" id="plot-world-map">

                    <div class="embed-responsive embed-responsive-4by3">    
                        <object data="http://polymorphixgaming.com:8124/"> 
                            Object could not be rendered. You can go to the dynmap manually <a href="http://polymorphixgaming.com:8124/">here</a>, but the server may be down.
                        </object>
                    </div>

                </div>

            </div>
            <!-- /IFRAME DYNAMP TAB FRAMES -->
        </div>
        <!-- /IFRAME DYNMAPS -->
        
        <div class="col-md-1"></div>
        
    </div>
    
</div>




