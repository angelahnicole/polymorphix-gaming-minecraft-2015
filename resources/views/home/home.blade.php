@extends('master.master')

@section('content')

    
<section class="window" id="minecraft">

    <div class="front-page-header"><div class="front-page-tag"><span class="glyphicon icon-minecraft"></div><h1>Minecraft Polymorphix Server <small>UM Programming Summer Camp 2015</small></h1></div>
    <div class="front-page-panel">
        
        @include('home.minecraft')
        
    </div> 

</section> <!-- /JUMBOTRON -->

<section class="window leaderboard-window" id="mmo">

    <div class="front-page-header"><div class="front-page-tag"><span class="glyphicon icon-mmo"></div><h1>Minecraft MMO <small>The Hub City</small></h1></div>
    <div class="front-page-panel">
        
        @include('leaderboard.mmo')
        
    </div>

</section> <!-- /MMO -->
 
<section class="window leaderboard-window" id="hunger-games">

    <div class="front-page-header"><div class="front-page-tag"><span class="glyphicon icon-hungergames"></span></div><h1>Minecraft Hunger Games <small>Player Versus Player</small></h1></div>
    <div class="front-page-panel">
        
        @include('leaderboard.hunger-games')
        
    </div> 
</section> <!-- /HUNGER GAMES -->

<section class="window leaderboard-window" id="plot-world">

    <div class="front-page-header"><div class="front-page-tag"><span class="glyphicon icon-plotworld"></span></div><h1>Minecraft Plot World <small>Creative Mode Central</small></h1></div>
    <div class="front-page-panel">  
        
        @include('leaderboard.plotworld')
        
    </div> 

</section> <!-- /PLOT WORLD -->

<section class="window leaderboard-window" id="server-commands">

    <div class="front-page-header"><div class="front-page-tag"><span class="glyphicon icon-terminal"></span></div><h1>Server Commands</h1></div>
    <div class="front-page-panel">
        
        @include('commands.commands')
        
    </div>

</section>  <!-- /SERVER COMMANDS -->


@stop

@section('scripts')

<script>

// ====================================================================================================================
// TOOLTIPS
// ====================================================================================================================
$(function () 
{
    $('[data-toggle="tooltip"]').tooltip();
});

// ====================================================================================================================
// MAKE NAV ELEMENT ACTIVE
// ====================================================================================================================
function makeActive(class_id)
{
    $('a.front-nav').removeClass('active');
    $( class_id ).addClass('active');
}

$(document).ready(function()
{    
    // ================================================================================================================
    // WAYPOINTS
    // ================================================================================================================
    
    var waypoint_offset = 50;
    
    var waypoint_minecraft = new Waypoint
    ({
        element: document.getElementById('minecraft'),
        handler: function() 
        {
            makeActive('#minecraft-nav');
        },
        offset: -waypoint_offset
    });
    
    var waypoint_mmo = new Waypoint
    ({
        element: document.getElementById('mmo'),
        handler: function() 
        {
            makeActive('#mmo-nav');
        },
        offset: waypoint_offset
    });
    
    var waypoint_hunger_games = new Waypoint
    ({
        element: document.getElementById('hunger-games'),
        handler: function() 
        {
            makeActive('#hunger-games-nav');
        },
        offset: waypoint_offset
    });
    
    var waypoint_plot_world = new Waypoint
    ({
        element: document.getElementById('plot-world'),
        handler: function() 
        {
            makeActive('#plot-world-nav');
        },
        offset: waypoint_offset
    });
    
    var waypoint_server_commands = new Waypoint
    ({
        element: document.getElementById('server-commands'),
        handler: function() 
        {
            makeActive('#server-commands-nav');
        },
        offset: waypoint_offset
    });
    
    // ================================================================================================================
    // DATA TABLES
    // ================================================================================================================
    
    $('#mmoTable').dataTable
    ({
        "scrollX": true,
        order: [[ $('#mmoSort').index(),  'desc' ]],
        searching: false
    });
    
    $('#pvpTable').dataTable
    ({
        "scrollX": true,
        order: [[ $('#pvpSort').index(),  'desc' ]],
        searching: false
    });
    
    $('#plotworldTable').dataTable
    ({
        "scrollX": true,
        order: [[ $('#plotworldSort').index(),  'desc' ]],
        searching: false
    });
    
      
});

</script>

@append