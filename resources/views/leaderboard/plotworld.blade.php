
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-1"></div>
        
        <div class="thumbnail with-caption col-md-10">
            <img class="img-responsive img-thumbnail" src="{{ URL::asset('assets/imx/frontpage/plot-world_photo.png') }}">
            <p><small>Image by <a href="http://meepcraft.wikia.com/wiki/Creative">MeepCraft Wikia</a></small></p>
        </div>
        
        <div class="col-md-1"></div>
    </div>
    
    <div class="row">
        <div class="col-md-1"></div>
        
        <!-- PLOTWORLD LEADERBOARD -->
        <div class="col-md-10">
            
            @if(isset($leaderboardsHidden) && !$leaderboardsHidden)
            
            <!-- PLOTWORLD LEADERBOARD TABLE -->
            <table class="table table-striped table-condensed" id="plotworldTable" cellspacing="0" width="100%">

                <colgroup>
                    <col class="width5" />
                    <col class="width5" />
                    <col class="width5" />
                    <col class="width5" />
                    <col class="width5" />
                </colgroup>

                <thead class="bigger-font">
                    <tr>
                        <th>User</th>
                        <th>Number of Votes</th>
                        <th>Average Vote</th>
                        <th class="defaultSort">Score</th>                        
                    </tr>
                </thead>

                <tbody>
                    @foreach($plotworldData as $plotworldScore)
                        <tr>
                            <td data-toggle="tooltip" data-container="body" data-placement="bottom" title="{!! $plotworldScore->user->minecraft_username !!}">
                                <img src="{!! $plotworldScore->user->minecraft_head_url !!}">
                            </td>
                            <td>{!! $plotworldScore->num_votes !!}</td>
                            <td>{!! $plotworldScore->avg_vote !!}</td>
                            <td class="defaultSort" id="plotworldSort">{!! $plotworldScore->score !!}</td> 
                        </tr>    
                    @endforeach

                </tbody>

            </table>
            <!-- /PLOTWORLD LEADERBOARD TABLE -->
            
        </div>
        <!-- /PLOTWORLD LEADERBOARD -->
        
        @else
        
        <p class="teaser">FIND OUT THE FINAL SCORE SOON!!</p>
        
        @endif
        
        <div class="col-md-1"></div>
    </div>
    
</div>

