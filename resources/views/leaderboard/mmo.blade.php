<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-1"></div>
        
        <div class="thumbnail with-caption col-md-10">
            <img class="img-responsive img-thumbnail" src="{{ URL::asset('assets/imx/frontpage/mmo_photo.jpg') }}">
            <p><small>Image (and world) by <a href="http://www.planetminecraft.com/project/eldaria-island---custom-terrain-mc-10---end-portal/">Aurelien_Sama</a></small></p>
        </div>
        
        <div class="col-md-1"></div>
    </div>
    
    <div class="row">
        <div class="col-md-1"></div>
        
        <div class="map-image col-md-10 text-center">  
            <button class="btn btn-primary bottom-margin" type="button" data-toggle="collapse" data-target="#map-collapse" aria-expanded="false" aria-controls="map-collapse">
                Click to expand the Eldaria Map
            </button>

            <div class="collapse" id="map-collapse">
                <div class="thumbnail with-caption">
                    <img class="img-responsive img-thumbnail" src="{{ URL::asset('assets/imx/frontpage/eldaria-map_photo.jpg') }}">
                    <p><small>Image from <a href="http://www.noelshack.com/2012-30-1343241863-eldaria-map-2d.jpg">Noel Shack</a></small></p>
                </div>
            </div>
        </div>
        
        <div class="col-md-1"></div>
    </div>
    
    <div class="row">
        <div class="col-md-1"></div>
        
        <!-- MMO LEADERBOARD -->
        <div class="col-md-10">
            
            @if(isset($leaderboardsHidden) && !$leaderboardsHidden)
            
            <!-- MMO LEADERBOARD TABLE -->
            <table class="table table-striped table-condensed" id="mmoTable" cellspacing="0" width="100%">

                <colgroup>
                    <col class="width16" />
                    <col class="width16" />
                    <col class="width16" />
                    <col class="width16" />
                    <col class="width16" />
                    <col class="width16" />
                    <col class="width16" />
                    <col class="width16" />
                    <col class="width16" />
                    <col class="width16" />
                    <col class="width16" />
                    <col class="width16" />
                    <col class="width16" />
                    <col class="width16" />
                    <col class="width16" />
                </colgroup>

                <thead class="bigger-font">
                    <tr>
                        <th>User</th>
                        <th data-toggle="tooltip" data-container="body" data-placement="top" title="Acrobatics"><span class="glyphicon icon-acrobatics"></span></th>
                        <th data-toggle="tooltip" data-container="body" data-placement="top" title="Alchemy"><span class="glyphicon icon-alchemy"></span></th>
                        <th data-toggle="tooltip" data-container="body" data-placement="top" title="Archery"><span class="glyphicon icon-archery"></span></th>
                        <th data-toggle="tooltip" data-container="body" data-placement="top" title="Axes"><span class="glyphicon icon-axes"></span></th>
                        <th data-toggle="tooltip" data-container="body" data-placement="top" title="Excavation"><span class="glyphicon icon-excavation"></span></th>
                        <th data-toggle="tooltip" data-container="body" data-placement="top" title="Fishing"><span class="glyphicon icon-fishing"></span></th>
                        <th data-toggle="tooltip" data-container="body" data-placement="top" title="Herbalism"><span class="glyphicon icon-herbalism"></span></th>
                        <th data-toggle="tooltip" data-container="body" data-placement="top" title="Mining"><span class="glyphicon icon-mining"></span></th>
                        <th data-toggle="tooltip" data-container="body" data-placement="top" title="Taming"><span class="glyphicon icon-taming"></span></th>
                        <th data-toggle="tooltip" data-container="body" data-placement="top" title="Repair"><span class="glyphicon icon-repair"></span></th>
                        <th data-toggle="tooltip" data-container="body" data-placement="top" title="Swords"><span class="glyphicon icon-swords"></span></th>   
                        <th data-toggle="tooltip" data-container="body" data-placement="top" title="Unarmed"><span class="glyphicon icon-unarmed"></span></th>
                        <th data-toggle="tooltip" data-container="body" data-placement="top" title="Wood Cutting"><span class="glyphicon icon-wood-cutting"></span></th>
                        <th>Score</th>
                        <th class="defaultSort">Balanced Score</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($mmoData as $mmoScore)
                        <tr>
                            <td data-toggle="tooltip" data-container="body" data-placement="bottom" title="{!! $mmoScore->user->minecraft_username !!}">
                                <img src="{!! $mmoScore->user->minecraft_head_url !!}">
                            </td>
                            <td>{!! $mmoScore->acrobatics !!}</td>
                            <td>{!! $mmoScore->alchemy !!}</td>
                            <td>{!! $mmoScore->archery !!}</td>
                            <td>{!! $mmoScore->axes !!}</td>
                            <td>{!! $mmoScore->excavation !!}</td>
                            <td>{!! $mmoScore->fishing !!}</td>
                            <td>{!! $mmoScore->herbalism !!}</td>
                            <td>{!! $mmoScore->mining !!}</td>
                            <td>{!! $mmoScore->taming !!}</td>
                            <td>{!! $mmoScore->repair !!}</td>             
                            <td>{!! $mmoScore->swords !!}</td>
                            <td>{!! $mmoScore->unarmed !!}</td>
                            <td>{!! $mmoScore->woodcutting !!}</td>
                            <td>{!! $mmoScore->score !!}</td> 
                            <td class="defaultSort" id="mmoSort">{!! $mmoScore->normalized_score !!}</td>
                        </tr>    
                    @endforeach

                </tbody>

            </table>
            <!-- /MMO LEADERBOARD TABLE -->
            
            @else
            
            <p class="teaser">FIND OUT THE FINAL SCORE SOON!!</p>
            
            @endif
            
        </div>
        <!-- /MMO LEADERBOARD -->
        
        <div class="col-md-1"></div>
    </div>
    
</div>


