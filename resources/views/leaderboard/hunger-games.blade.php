
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-1"></div>
        
        <div class="thumbnail with-caption col-md-10">
            <img class="img-responsive img-thumbnail" src="{{ URL::asset('assets/imx/frontpage/hunger-games_photo.png') }}">
            <p><small>Image by <a href="http://aliciamyerscough.deviantart.com/art/Minecraft-Sunset-Balcony-HD-Render-Wallpaper-544439470">AliciaMyerscough</a></small></p>
        </div>
        
        <div class="col-md-1"></div>
    </div>
    
    <div class="row">
        <div class="col-md-1"></div>

        <!-- PVP LEADERBOARD -->
        <div class="col-md-10">
            
            @if(isset($leaderboardsHidden) && !$leaderboardsHidden)
            
            <!-- PVP LEADERBOARD TABLE -->
            <table class="table table-striped table-condensed" id="pvpTable" cellspacing="0" width="100%">

                <colgroup>
                    <col class="width6" />
                    <col class="width6" />
                    <col class="width6" />
                    <col class="width6" />
                    <col class="width6" />
                </colgroup>

                <thead class="bigger-font">
                    <tr>
                        <th>User</th>
                        <th>Kills</th>
                        <th>Deaths</th>
                        <th>Wins</th>
                        <th>Score</th>      
                        <th class="defaultSort">Balanced Score</th> 
                    </tr>
                </thead>

                <tbody>
                    @foreach($pvpData as $pvpScore)
                        <tr>
                            <td data-toggle="tooltip" data-container="body" data-placement="bottom" title="{!! $pvpScore->user->minecraft_username !!}">
                                <img src="{!! $pvpScore->user->minecraft_head_url !!}">
                            </td>
                            <td>{!! $pvpScore->kills !!}</td>
                            <td>{!! $pvpScore->deaths !!}</td>
                            <td>{!! $pvpScore->wins !!}</td>
                            <td>{!! $pvpScore->score !!}</td> 
                            <td class="defaultSort" id="pvpSort">{!! $pvpScore->normalized_score !!}</td> 
                        </tr>    
                    @endforeach

                </tbody>

            </table>
            <!-- /PVP LEADERBOARD TABLE -->
            
            @else
            
            <p class="teaser">FIND OUT THE FINAL SCORE SOON!!</p>
            
            @endif

        </div>
        <!-- /PVP LEADERBOARD -->

        <div class="col-md-1"></div>
    </div>
</div>

