<?php namespace App\Libraries;

use DB;
use App\Models\User;
use App\Models\MMOLeaderboard;
use App\Models\PVPLeaderboard;
use App\Models\PlotworldLeaderboard;

// ===================================================================================================================================
// MinecraftAPI.php
// -----------------------------------------------------------------------------------------------------------------------------------
// Polymorphix Gaming
// https://bitbucket.org/angelahnicole/
// @author Angela Gross
// -----------------------------------------------------------------------------------------------------------------------------------
// Controller for the main pages of minecraft
// ===================================================================================================================================

class MinecraftAPI
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    // STATIC CLASS
    private function __construct() {}
     
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    // ==========================================================================================================
    // UPDATE USERS
    // ----------------------------------------------------------------------------------------------------------
    // @summary Retrieves user information from the minecraft database (from mcmmo) and imports it into the 
    // polmorphix user table.
    // @return void
    // ==========================================================================================================
    public static function updateUsers()
    {
        $minecraft_head_URL = 'https://minotar.net/helm/%s/25.png';
        $minecraft_username = 'user';
        $minecraft_uuid = 'uuid';
        $minecraft_online_time = 'OnlineTime';
        
        $minecraftUsers = DB::connection('minecraft')->select
        (
                'SELECT 
                    mcmmo_users.user, 
                    mcmmo_users.uuid, 
                    IFNULL(BungeeOnlineTime.OnlineTime, 1) AS OnlineTime
                FROM mcmmo_users
                LEFT JOIN BungeeOnlineTime ON BungeeOnlineTime.uuid = mcmmo_users.uuid'
        );
        
        foreach($minecraftUsers as $user)
        {
            $webUser = User::firstOrCreate
            ( 
                [ 
                    'minecraft_uuid'        => str_replace('-', '', $user->$minecraft_uuid),
                    'minecraft_username'    => $user->$minecraft_username,
                    'minecraft_head_url'    => sprintf($minecraft_head_URL, $user->$minecraft_username)
                ] 
            );

            $webUser->online_time = ($user->$minecraft_online_time == 1 || $user->$minecraft_online_time < 240) ? 240 : (4 * $user->$minecraft_online_time);
            
            $webUser->save();
        }       
    }
    
    // ==========================================================================================================
    // UPDATE MMO LEADERBOARD
    // ----------------------------------------------------------------------------------------------------------
    // @summary Retrieves MMO experience information from the minecraft database (from mcmmo) and imports it
    // into the polymorphix mmo leaderboard table.
    // @return void
    // ==========================================================================================================
    public static function updateMMOLeaderboard()
    {
        $minecraft_uuid = 'uuid';
        $minecraft_total_experience = 'total_experience';
        $user_id = 'user_id';
        
        // Retrieve experience information for all users on the minecraft database
        $minecraftMMOUsers = DB::connection('minecraft')->select
        (
            'SELECT 
                users.uuid,
                exp.taming, exp.mining, exp.woodcutting, exp.repair, exp.unarmed, 
                exp.herbalism, exp.excavation, exp.archery, exp.swords, exp.axes,
                exp.acrobatics , exp.fishing, exp.alchemy,
                ( 
                        exp.taming + exp.mining + exp.woodcutting + exp.repair + exp.unarmed + 
                        exp.herbalism + exp.excavation + exp.archery + exp.swords + exp.axes + 
                        exp.acrobatics + exp.fishing + exp.alchemy 
                ) AS total_experience
            FROM mcmmo_users AS users
            INNER JOIN mcmmo_experience AS exp ON exp.user_id = users.id'
        );
        
        foreach($minecraftMMOUsers as $user)
        {
            // Retrieve the user on the web database based on UUID
            $webUser = User::where('minecraft_uuid', str_replace('-', '', $user->$minecraft_uuid))->first();
   
            // If the retrieved user is non-null, retrieve/create the score entry and update with MMO experience, and
            // save.
            if($webUser != null)
            {
                $webMMOScore = MMOLeaderboard::firstOrCreate( [ 'user_id' => $webUser->$user_id ] );
                
                $scoreData = 
                [
                    'normalized_score' => $user->$minecraft_total_experience / $webUser->online_time,
                    'score' => $user->$minecraft_total_experience,
                    'taming' => $user->taming,
                    'mining' => $user->mining,
                    'woodcutting' => $user->woodcutting,
                    'repair' => $user->repair,
                    'unarmed' => $user->unarmed,
                    'herbalism' => $user->herbalism,
                    'excavation' => $user->excavation,
                    'archery' => $user->archery,
                    'swords' => $user->swords,
                    'axes' => $user->axes,
                    'acrobatics' => $user->acrobatics,
                    'fishing' => $user->fishing,
                    'alchemy' => $user->alchemy                    
                ];
                
                $webMMOScore->fill($scoreData);
                            
                $webMMOScore->save();
            }
        }
    }
    
    // ==========================================================================================================
    // UPDATE PVP LEADERBOARD
    // ----------------------------------------------------------------------------------------------------------
    // @summary Retrieves PVP kill information from the minecraft database (from bg) and imports it into the
    // polymorphix pvp leaderboard table.
    // @return void
    // ==========================================================================================================
    public static function updatePVPLeaderboard()
    {
        $minecraft_kills = 'KILLS';
        $minecraft_deaths = 'DEATHS';
        $minecraft_wins = 'WINS';
        $minecraft_uuid = 'UUID';
        $user_id = 'user_id';
        
        // Retrieve PVP information for all users on the minecraft database
        $minecraftPVPUsers = DB::connection('minecraft')->select
        (
            'SELECT 
                players.UUID,
                (
                        SELECT COUNT(*)
                        FROM bg_plays
                        WHERE REF_KILLER = players.ID
                ) AS KILLS,
                (
                        SELECT COUNT(*)
                        FROM bg_plays
                        WHERE REF_PLAYER = players.ID
                        AND DEATH_REASON != \'WINNER\'
                ) AS DEATHS,
                (
                        SELECT COUNT(*)
                        FROM bg_plays
                        WHERE REF_PLAYER = players.ID
                        AND DEATH_REASON = \'WINNER\'
                ) AS WINS
            FROM bg_players AS players'
        );
        
        foreach($minecraftPVPUsers as $user)
        {
            // Retrieve the user on the web database based on UUID
            $webUser = User::where('minecraft_uuid', $user->$minecraft_uuid)->first();           
            
            // If the retrieved user is non-null, retrieve/create the score entry and update with PVP information, and
            // save.
            if($webUser != null)
            {
                $webPVPScore = PVPLeaderboard::firstOrCreate( [ 'user_id' => $webUser->$user_id ] );
                $webPVPScore->kills = $user->$minecraft_kills;
                $webPVPScore->deaths = $user->$minecraft_deaths; 
                $webPVPScore->wins = $user->$minecraft_wins; 
                $webPVPScore->score = $user->$minecraft_wins;
                $webPVPScore->normalized_score = $user->$minecraft_wins / $webUser->online_time;
                $webPVPScore->save();
            }
        }
    }
    
    // ==========================================================================================================
    // UPDATE PLOTWORLD LEADERBOARD
    // ----------------------------------------------------------------------------------------------------------
    // @summary Retrieves user rating informaton from the minecraft database (from plot) and imports it into the
    // polymorphix plotworld leaderboard table.
    // @return 
    // ==========================================================================================================
    public static function updatePlotworldLeaderboard()
    {
        $minecraft_uuid = 'uuid';
        $minecraft_avg_rating = 'avg_rating';
        $minecraft_num_rating = 'num_rating';
        $user_id = 'user_id';
        
        $minecraftPlotUsers = DB::connection('minecraft')->select
        (
            'SELECT
                plot_plot.owner AS uuid,
                plot_plot_rating.plot_plot_id,
                AVG(1.0 * plot_plot_rating.rating) AS avg_rating,
                COUNT(plot_plot_rating.rating) AS num_rating
            FROM plot_plot
            INNER JOIN plot_plot_rating ON plot_plot_rating.plot_plot_id = plot_plot.id
            GROUP BY plot_plot_rating.plot_plot_id;'
        );
        
        foreach($minecraftPlotUsers as $user)
        {
            // Retrieve the user on the web database based on UUID
            $webUser = User::where('minecraft_uuid', str_replace('-', '', $user->$minecraft_uuid))->first();
            
            // If the retrieved user is non-null, retrieve/create the score entry and update with plot rating 
            // information, and save.
            if($webUser != null)
            {
                $webPlotScore = PlotworldLeaderboard::firstOrCreate( [ 'user_id' => $webUser->$user_id ] );   
                
                $plotData =
                [
                    'num_votes' => $user->$minecraft_num_rating,
                    'avg_vote' => number_format($user->$minecraft_avg_rating, 2),
                    'score' => number_format($user->$minecraft_avg_rating, 2)
                ];
                
                $webPlotScore->fill($plotData);
                
                $webPlotScore->save();
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
