<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// ===================================================================================================================================
// User.php
// -----------------------------------------------------------------------------------------------------------------------------------
// Polymorphix Gaming
// https://bitbucket.org/angelahnicole/
// @author Angela Gross
// -----------------------------------------------------------------------------------------------------------------------------------
// Model for the 'user' table.
// ===================================================================================================================================

class User extends Model
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    // ------------------------------------------------------------------------------------------------------------------------------
    // ATTRIBUTES
    // ------------------------------------------------------------------------------------------------------------------------------
    protected $table = 'user';
    protected $primaryKey = 'user_id'; 
    protected $fillable = ['minecraft_uuid', 'minecraft_head_url', 'minecraft_username', 'online_time'];
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
        
    // ------------------------------------------------------------------------------------------------------------------------------
    // RELATIONSHIPS
    // ------------------------------------------------------------------------------------------------------------------------------
    
    public function mmoLeaderboard()
    {
        return $this->hasOne('App\Models\MMOLeaderboard', 'user_id');
    }
    
    public function pvpLeaderboard()
    {
        return $this->hasOne('App\Models\PVPLeaderboard', 'user_id');
    }
    
    public function plotworldLeaderboard()
    {
        return $this->hasOne('App\Models\Plotworld', 'user_id');
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
}
