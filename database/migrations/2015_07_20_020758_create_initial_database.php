<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// ===================================================================================================================================
// CreateInitialDatabase.php
// -----------------------------------------------------------------------------------------------------------------------------------
// Polymorphix Gaming
// https://bitbucket.org/angelahnicole/
// @author Angela Gross
// -----------------------------------------------------------------------------------------------------------------------------------
//
// ===================================================================================================================================

class CreateInitialDatabase extends Migration
{
   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    // ==========================================================================================================
    // RUN MIGRATIONS    
    // ==========================================================================================================
    public function up()
    {
        // ------------------------------------------------------------------------------------------------------
        // USER -> Drop table and create it
        // ------------------------------------------------------------------------------------------------------
        Schema::dropIfExists('user');
        Schema::create('user', function (Blueprint $table)
        {
            $table->engine='InnoDB';
            $table->increments('user_id');
            $table->string('minecraft_username')->unique();
            $table->string('minecraft_uuid')->unique();
            $table->string('minecraft_head_url')->unique();
            $table->timestamps();
        });
        
        // ------------------------------------------------------------------------------------------------------
        // MMO LEADERBOARD -> Drop table and create it
        // ------------------------------------------------------------------------------------------------------
        Schema::dropIfExists('mmo_leaderboard');
        Schema::create('mmo_leaderboard', function (Blueprint $table)
        {
            $table->engine='InnoDB';
            $table->increments('mmo_leaderboard_id');
            $table->integer('score');
            $table->float('normalized_score');
            $table->integer('taming');
            $table->integer('mining');
            $table->integer('woodcutting');
            $table->integer('repair');
            $table->integer('unarmed');
            $table->integer('herbalism');
            $table->integer('excavation');
            $table->integer('archery');
            $table->integer('swords');
            $table->integer('axes');
            $table->integer('acrobatics');
            $table->integer('fishing');
            $table->integer('alchemy');         
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('user');
            $table->timestamps();
        });
        
        // ------------------------------------------------------------------------------------------------------
        // PVP LEADERBOARD -> Drop table and create it
        // ------------------------------------------------------------------------------------------------------
        Schema::dropIfExists('pvp_leaderboard');
        Schema::create('pvp_leaderboard', function (Blueprint $table)
        {
            $table->engine='InnoDB';
            $table->increments('pvp_leaderboard_id');
            $table->integer('kills');
            $table->integer('deaths');
            $table->integer('wins');
            $table->integer('score');
            $table->float('normalized_score');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('user');
            $table->timestamps();
        });
        
        // ------------------------------------------------------------------------------------------------------
        // PLOTWORLD LEADERBOARD -> Drop table and create it
        // ------------------------------------------------------------------------------------------------------
        Schema::dropIfExists('plotworld_leaderboard');
        Schema::create('plotworld_leaderboard', function (Blueprint $table)
        {
            $table->engine='InnoDB';
            $table->increments('plotworld_leaderboard_id');
            $table->integer('num_votes');
            $table->float('avg_vote');
            $table->float('score');    
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('user');
            $table->timestamps();
        });
    }
    

   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // ==========================================================================================================
    // REVERSE MIGRATIONS
    // ----------------------------------------------------------------------------------------------------------
    // Drops all the tables in the proper order to avoid an integrity constraint violation
    // ==========================================================================================================
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('user');
        Schema::dropIfExists('mmo_leaderboard');       
        Schema::dropIfExists('pvp_leaderboard');
        Schema::dropIfExists('plotworld_leaderboard');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
